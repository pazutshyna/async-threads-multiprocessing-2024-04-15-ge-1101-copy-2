import time
import httpx
from bs4 import BeautifulSoup

URL = "https://djinni.co/jobs/"                             # All


def get_djinni_jobs(page: int, client: httpx.Client, url: str = URL):
    response = client.get(url, params={"page": page})
    soup = BeautifulSoup(response.content, "html.parser")
    return [
        job.text.strip() for job in soup.findAll("a", class_="h3 job-list-item__link")
    ]


def main(start_page: int, stop_page: int) -> None:
    with httpx.Client() as client:
        for page in range(start_page, stop_page):
            print(get_djinni_jobs(page, client, URL))


def get_djinni_jobs_without_client(page: int, url: str = URL):
    response = httpx.get(url, params={"page": page})
    soup = BeautifulSoup(response.content, "html.parser")
    return [
        job.text.strip() for job in soup.findAll("a", class_="h3 job-list-item__link")
    ]


def main_without_client(start_page: int, stop_page: int) -> None:
    for page in range(start_page, stop_page):
        print(get_djinni_jobs_without_client(page, URL))


if __name__ == "__main__":
    start_page = 10
    stop_page = 60
    # start = time.perf_counter()
    # main(start_page, stop_page)
    # duration = time.perf_counter() - start
    # print(f"Duration: {duration}")

    start = time.perf_counter()
    main_without_client(start_page, stop_page)
    duration = time.perf_counter() - start
    print(f"Duration: {duration}")
